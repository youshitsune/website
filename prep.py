#! /usr/bin/env python

from datetime import datetime
from functools import cmp_to_key

days = [
    "Ponedeljak",
    "Utorak",
    "Sreda",
    "Četvrtak",
    "Petak",
    "Subota",
    "Nedelja",
]

months = [
    "Januar",
    "Februar",
    "Mart",
    "April",
    "Maj",
    "Jun",
    "Jul",
    "Avgust",
    "Septembar",
    "Oktobar",
    "Novembar",
    "Decembar",
]

today = datetime.today().date()

def parse_date(date):
    return datetime.strptime(date,"%d-%m-%Y").date()

def compare_events(one, two):
    one = parse_date(one.split(", ")[0])
    two = parse_date(two.split(", ")[0])
    if one>two:
        return 1
    elif one==two:
        return 0
    else:
        return -1

def is_past_event(event):
    return event < today

def load_events():
    events = []
    with open("dogadjaji.csv", "rt") as file:
        file.readline()
        for event in file.readlines():
            event = event.strip()
            if event != "":
                events.append(event)
    return events

def write_events(events):
    with open("dogadjaji.csv", "wt") as file:
        file.write("datum, vreme, lokacija, tema\n")
        for event in events:
            file.write(event+"\n")

def sort_events(events):
    return sorted(events, key = cmp_to_key(compare_events))


events = load_events()
events = sort_events(events)
write_events(events)

newevents = []

for event in events:
    date, time, location, title = event.split(", ")
    date = parse_date(date)
    if is_past_event(date):
        continue
    date = days[date.weekday()]+", "+str(date.day)+". "+months[date.month-1]+" "+str(date.year)+"."
    time = time+"h"
    future_event = []
    future_event.append("<td> "+date+" </td>")
    future_event.append("<td> "+time+" </td>")
    if "https://" in location:
        place,link = location.split("https://")
        future_event.append("<td> <a href=\"https://"+link+"\""+"> "+place.strip()+" </a> </td>")
    else:
        future_event.append("<td> "+location.strip()+" </td>")
    future_event.append("<td> "+title+" </td>")
    newevents.append("<tr>\n"+"\n".join(future_event)+"\n</tr>")

with open("pages/sr/events.html","wt") as file:
    file.writelines(["<h1>Događaji</h1>\n", "<table>\n", "<tr><th>Datum</th><th>Vreme</th><th>Mesto</th><th>Tema</th></tr>\n"])
    file.writelines(newevents)
    file.writelines(["</table>"])

#with open("pages/en/events.html","wt") as file:
#    file.writelines(["<h1>Events</h1>", "<table>", "<tr>\n<th>Date</th>\n<th>Time</th>\n<th>Place</th>\n<th>Theme</th>\n</tr>"])
#    file.writelines(events)
#    file.writelines(["</table>"])
